package eu.pintergabor.common;

/**
 * Anything that has a position as float coordinates.
 */
@SuppressWarnings("unused")
public interface HasPositionF {

    /**
     * Get the X position of something.
     *
     * @return The X position of something.
     */
    float getX();

    /**
     * Set the X position of something.
     *
     * @param x The X position of something.
     */
    void setX(float x);

    /**
     * Get the Y position of something.
     *
     * @return The Y position of something.
     */
    float getY();

    /**
     * Set the Y position of something.
     *
     * @param y The Y position of something.
     */
    void setY(float y);

}
