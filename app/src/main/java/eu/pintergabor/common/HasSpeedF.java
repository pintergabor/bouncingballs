package eu.pintergabor.common;

/**
 * Anything that has a speed expressed as X, Y float values.
 */
@SuppressWarnings("unused")
public interface HasSpeedF {

    /**
     * Get the X speed of something.
     *
     * @return The X speed of something.
     */
    float getXSpeed();

    /**
     * Set the X speed of something.
     *
     * @param xSpeed The X speed of something.
     */
    void setXSpeed(float xSpeed);

    /**
     * Get the Y speed of something.
     *
     * @return The Y speed of something.
     */
    float getYSpeed();

    /**
     * Set the Y speed of something.
     *
     * @param ySpeed The Y speed of something.
     */
    void setYSpeed(float ySpeed);

}
