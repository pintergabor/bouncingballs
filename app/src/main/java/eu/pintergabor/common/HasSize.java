package eu.pintergabor.common;

/**
 * Anything that has a size.
 */
@SuppressWarnings("unused")
public interface HasSize {

    /**
     * Get the width of something.
     *
     * @return The width of something.
     */
    int getWidth();

    /**
     * Set the width of something.
     *
     * @param width The width of something.
     */
    void setWidth(int width);

    /**
     * Get the height of something.
     *
     * @return The height of something.
     */
    int getHeight();

    /**
     * Set the height.
     *
     * @param height The height of something.
     */
    void setHeight(int height);

}
