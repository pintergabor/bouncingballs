package eu.pintergabor.bouncingballs.balls;

import eu.pintergabor.bouncingballs.ball.ball.Ball;
import eu.pintergabor.bouncingballs.com.MainHub;

/**
 * Balls data.
 * <p>
 * {@link BallsMove} > {@link BallsInit} > {@link BallsDraw} > {@link BallsData}.
 */
public class BallsData {

    @SuppressWarnings("unused")
    static final String TAG = "BALL";

    /**
     * The balls.
     */
    public Ball[] balls;

    /**
     * Create all balls with zero size, position, and speed.
     */
    public BallsData() {
        final MainHub mainHub = MainHub.getInstance();
        final int size = mainHub.nBalls;
        balls = new Ball[size];
        for (int i = 0; i < size; i++) {
            // One ball.
            balls[i] = new Ball();
        }
    }

    /**
     * Make a deep copy.
     *
     * @param balls Source to copy from.
     */
    @SuppressWarnings("unused")
    public void clone(BallsData balls) {
        final MainHub mainHub = MainHub.getInstance();
        final int size = mainHub.nBalls;
        for (int i = 0; i < mainHub.nBalls; i++) {
            this.balls[i].clone(balls.balls[i]);
        }
    }

}
