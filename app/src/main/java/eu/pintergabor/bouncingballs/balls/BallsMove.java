package eu.pintergabor.bouncingballs.balls;

import android.util.SparseArray;

import java.util.Random;

import eu.pintergabor.bouncingballs.ball.ball.Ball;
import eu.pintergabor.bouncingballs.com.MainHub;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;
import eu.pintergabor.util.gravity.GravityRecorder;
import eu.pintergabor.util.touch.TouchPoint;
import eu.pintergabor.util.touch.TouchRecorder;

import static eu.pintergabor.bouncingballs.global.Const.GLIMIT;

/**
 * The balls.
 * <p>
 * {@link BallsMove} > {@link BallsInit} > {@link BallsDraw} > {@link BallsData}.
 */
public class BallsMove extends BallsInit {

    /**
     * Random number generator.
     */
    private Random rnd = new Random();

    /**
     * Calculate the new position of all the balls in an ideal world.
     * Lossless bouncing and no drag.
     *
     * @param millis After this milliseconds.
     */
    public void moveBalls(int millis) {
        final ManageViews manageViews = ManageViews.getInstance();
        final int frameWidth = manageViews.getFrameWidth();
        final int frameHeight = manageViews.getFrameHeight();
        for (Ball ball : balls) {
            // Compensate for ball size.
            final float left = ball.getWidth() / 2f;
            final float right = frameWidth - ball.getWidth() / 2f;
            final float width1 = right - left;
            final float width2 = 2 * width1;
            final float top = ball.getHeight() / 2f;
            final float bottom = frameHeight - ball.getHeight() / 2f;
            final float height1 = bottom - top;
            final float height2 = 2 * height1;
            // Sanity check.
            if (left < right && top < bottom) {
                // New position of the ball in a folded, aliased space, where it cannot bounce.
                float dx = ball.getXSpeed() * millis / 1000f;
                float dy = ball.getYSpeed() * millis / 1000f;
                float x = ball.getX() + dx;
                float y = ball.getY() + dy;
                float xSpeed = ball.getXSpeed();
                float ySpeed = ball.getYSpeed();
                // -offset
                x -= left;
                y -= top;
                // Unaliasing
                while (x < 0) {
                    x += width2;
                }
                while (width2 <= x) {
                    x -= width2;
                }
                while (y < 0) {
                    y += height2;
                }
                while (height2 <= y) {
                    y -= height2;
                }
                // Unfolding
                if (width1 < x) {
                    xSpeed = -xSpeed;
                    x = width2 - x;
                }
                if (height1 < y) {
                    ySpeed = -ySpeed;
                    y = height2 - y;
                }
                // +offset
                x += left;
                y += top;
                // Set
                ball.setX(x);
                ball.setY(y);
                ball.setXSpeed(xSpeed);
                ball.setYSpeed(ySpeed);
            }
        }
    }

    /**
     * Drag reduces speed.
     *
     * @param millis After this milliseconds.
     */
    public void dragBalls(int millis) {
        final ManageViews manageViews = ManageViews.getInstance();
        final float density = manageViews.getFrameDensity();
        final MainHub mainHub = MainHub.getInstance();
        final double drag = mainHub.nDrag * density * millis / 1000f;
        for (Ball ball : balls) {
            double xSpeed = ball.getXSpeed();
            double ySpeed = ball.getYSpeed();
            double speed = Math.hypot(xSpeed, ySpeed);
            if (0 < speed) {
                final double xdSpeed = drag * (xSpeed / speed);
                if (xdSpeed < xSpeed || xSpeed < -xdSpeed) {
                    xSpeed -= xdSpeed;
                } else {
                    // Drag cannot change the direction of movement.
                    xSpeed = 0;
                }
                final double ydSpeed = drag * (ySpeed / speed);
                if (ydSpeed < ySpeed || ySpeed < -ydSpeed) {
                    ySpeed -= ydSpeed;
                } else {
                    // Drag cannot change the direction of movement.
                    ySpeed = 0;
                }
            }
            // Set
            ball.setXSpeed((float) xSpeed);
            ball.setYSpeed((float) ySpeed);
        }
    }

    /**
     * Drift changes speed randomly.
     *
     * @param millis After this milliseconds.
     */
    public void driftBalls(int millis) {
        final ManageViews manageViews = ManageViews.getInstance();
        final float density = manageViews.getFrameDensity();
        final MainHub mainHub = MainHub.getInstance();
        // Make drift 10x strong
        final float drift = mainHub.nDrift * density * millis / 200f;
        for (Ball ball : balls) {
            // Apply drift independently to both X and Y directions to speed up calculations.
            // This is cheating. Like approximating a circle with a square.
            final float dxSpeed = drift * (rnd.nextFloat() - 0.5f);
            final float dySpeed = drift * (rnd.nextFloat() - 0.5f);
            ball.setXSpeed(ball.getXSpeed() + dxSpeed);
            ball.setYSpeed(ball.getYSpeed() + dySpeed);
        }
    }

    /**
     * Gravity speeds things down.
     *
     * @param millis After this milliseconds.
     */
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public void gravityBalls(int millis) {
        final ManageViews manageViews = ManageViews.getInstance();
        final float density = manageViews.getFrameDensity();
        final MainHub mainHub = MainHub.getInstance();
        final float gravity = mainHub.nGravity * density * millis / 1000f;
        final GravityRecorder gravityRecorder = GravityRecorder.getInstance();
        float dxSpeed = 0;
        float dySpeed = 0;
        synchronized (gravityRecorder) {
            // Ignore low values and allow balls to free float when the device is on a level surface.
            final float gx = gravityRecorder.getGX();
            if (GLIMIT < Math.abs(gx)) {
                dxSpeed = gx * gravity;
            }
            final float gy = gravityRecorder.getGY();
            if (GLIMIT < Math.abs(gy)) {
                dySpeed = gy * gravity;
            }
        }
        for (Ball ball : balls) {
            // Set
            ball.setXSpeed(ball.getXSpeed() + dxSpeed);
            ball.setYSpeed(ball.getYSpeed() + dySpeed);
        }
    }

    /**
     * Touch pulls things.
     *
     * @param millis After this milliseconds.
     */
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public void touchBalls(int millis) {
        final ManageViews manageViews = ManageViews.getInstance();
        final int frameWidth = manageViews.getFrameWidth();
        final int frameHeight = manageViews.getFrameHeight();
        final float density = manageViews.getFrameDensity();
        final MainHub mainHub = MainHub.getInstance();
        final float spring = mainHub.nTouch * density * millis / 1000f / Math.min(frameWidth, frameHeight);
        final TouchRecorder touchRecorder = TouchRecorder.getInstance();
        for (Ball ball : balls) {
            float dxSpeed = 0;
            float dySpeed = 0;
            synchronized (touchRecorder) {
                final SparseArray<TouchPoint> touches = touchRecorder.touches;
                for (int i = 0; i < touches.size(); i++) {
                    TouchPoint p = touches.valueAt(i);
                    final float dx = ball.getX() - p.x;
                    final float dy = ball.getY() - p.y;
                    dxSpeed -= dx * spring;
                    dySpeed -= dy * spring;
                }
            }
            // Set
            ball.setXSpeed(ball.getXSpeed() + dxSpeed);
            ball.setYSpeed(ball.getYSpeed() + dySpeed);
        }
    }

}
