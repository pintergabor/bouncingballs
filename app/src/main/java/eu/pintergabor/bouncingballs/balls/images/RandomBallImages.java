package eu.pintergabor.bouncingballs.balls.images;

import eu.pintergabor.bouncingballs.ball.balldrawable.FancyBallDrawable;
import eu.pintergabor.bouncingballs.ball.ballimage.DrawableBallImage;
import eu.pintergabor.bouncingballs.ball.ballimage.ShadowBallImage;
import eu.pintergabor.bouncingballs.ball.ballimage.SimpleBallImage;
import eu.pintergabor.bouncingballs.global.Const;
import eu.pintergabor.bouncingballs.ui.MainActivity;

/**
 * An array of randomly generated, identical size ball images of
 * type {@link DrawableBallImage} or {@link SimpleBallImage}.
 */
public class RandomBallImages extends BallImagesBase {

    /**
     * Create empty ball images.
     *
     * @param number Number of images.
     */
    @SuppressWarnings("unused")
    public RandomBallImages(int number) {
        super(number);
    }

    /**
     * Create random ball images.
     *
     * @param number    Number of images.
     * @param ballsize  Ball size in dp.
     * @param imagetype Either {@link Const#BALL_IMAGE_SIMPLE} or {@link Const#BALL_IMAGE_FANCY}.
     */
    @SuppressWarnings("unused")
    public RandomBallImages(int number, int ballsize, int imagetype) {
        super(number);
        create(ballsize, imagetype);
    }

    /**
     * Create random images.
     *
     * @param ballsize  Ball size in dp.
     * @param imagetype Either {@link Const#BALL_IMAGE_SIMPLE} or {@link Const#BALL_IMAGE_FANCY}.
     */
    public void create(int ballsize, int imagetype) {
        final MainActivity mainActivity = MainActivity.getMainActivity();
        final float density = mainActivity.getResources().getDisplayMetrics().density;
        // Make some ball images.
        for (int i = 0; i < getSize(); i++) {
            float hue = (float) (360f * Math.random());
            switch (imagetype) {
                case Const.BALL_IMAGE_SIMPLE:
                    // A simple ball is just a simple ball.
                    ballImages[i] =
                        new SimpleBallImage(density,
                            ballsize, hue, 1.0f);
                    break;
                case Const.BALL_IMAGE_FANCY:
                    // A fancy ball is a drawable ball
                    // enclosing a fancy drawable ball.
                    ballImages[i] = new ShadowBallImage(
                        new DrawableBallImage(
                            new FancyBallDrawable(density,
                                ballsize, hue, 0.5f, 1.0f)));
                    break;
            }
        }
    }

}
