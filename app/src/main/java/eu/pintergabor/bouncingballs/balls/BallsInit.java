package eu.pintergabor.bouncingballs.balls;

import java.util.Random;

import eu.pintergabor.bouncingballs.com.MainHub;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;

/**
 * Initialize balls.
 * <p>
 * {@link BallsMove} > {@link BallsInit} > {@link BallsDraw} > {@link BallsData}.
 */
public class BallsInit extends BallsDraw {

    /**
     * Create and initialize balls.
     */
    public BallsInit(){
        super();
        init();
    }

    /**
     * Initialize balls.
     */
    public void init() {
        // Access frame size and parameters.
        final ManageViews manageViews = ManageViews.getInstance();
        final int width = manageViews.getFrameWidth();
        final int height = manageViews.getFrameHeight();
        final float density = manageViews.getFrameDensity();
        final MainHub mainHub = MainHub.getInstance();
        // Need a random number generator.
        final Random rnd = new Random();
        // Adjust max speed.
        final float maxspeed = mainHub.nBallSpeed * density;
        for (int i = 0; i < mainHub.nBalls; i++) {
            // Logical size is the same as display size.
            balls[i].setWidth(mainHub.ballsToImages[i].getWidth());
            balls[i].setHeight(mainHub.ballsToImages[i].getHeight());
            // Try to place them at the center middle of the enclosing View.
            balls[i].setX(Math.max(balls[i].getWidth() / 2, width / 2));
            balls[i].setY(Math.max(balls[i].getHeight() / 2, height / 2));
            // And give them a random speed.
            balls[i].setXSpeed(2 * maxspeed * rnd.nextFloat() - maxspeed);
            balls[i].setYSpeed(2 * maxspeed * rnd.nextFloat() - maxspeed);
        }
    }

}
