package eu.pintergabor.bouncingballs.balls.images;

import eu.pintergabor.bouncingballs.ball.ballimage.BallImageInterface;
import eu.pintergabor.bouncingballs.ball.ballimage.DrawableBallImage;
import eu.pintergabor.bouncingballs.ball.ballimage.SimpleBallImage;

/**
 * An array of ball images.
 */
public abstract class BallImagesBase {

    /**
     * See {@link #getSize()}.
     */
    private int size;

    /**
     * Number of different ball images.
     */
    public int getSize() {
        return size;
    }

    /**
     * See {@link #getBallImage(int)}.
     */
    protected BallImageInterface[] ballImages;

    /**
     * Get ball image.
     * <p>
     * It can be a {@link DrawableBallImage} or a {@link SimpleBallImage},
     * or anything else that implements the {@link BallImageInterface}.
     *
     * @param index Which ball.
     */
    public BallImageInterface getBallImage(int index) {
        return ballImages[index];
    }

    /**
     * Create empty ball images.
     *
     * @param number Number of images.
     */
    public BallImagesBase(int number) {
        size = number;
        ballImages = new BallImageInterface[size];
    }

}
