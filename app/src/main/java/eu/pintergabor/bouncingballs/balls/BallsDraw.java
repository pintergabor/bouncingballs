package eu.pintergabor.bouncingballs.balls;

import android.graphics.Canvas;

import eu.pintergabor.bouncingballs.com.MainHub;

import static android.graphics.Color.WHITE;

/**
 * Draw te balls on a canvas.
 * <p>
 * {@link BallsMove} > {@link BallsInit} > {@link BallsDraw} > {@link BallsData}.
 */
public class BallsDraw extends BallsData {

    /**
     * Draw the balls on the canvas.
     *
     * @param canvas Canvas to draw on.
     */
    public void draw(Canvas canvas) {
        // Background
        canvas.drawColor(WHITE);
        // Balls
        final MainHub mainHub = MainHub.getInstance();
        if (balls != null) {
            for (int i = 0; i < mainHub.nBalls; i++) {
                mainHub.ballsToImages[i].draw(canvas,
                    (int) balls[i].getX(), (int) balls[i].getY());
            }
        }
    }

}
