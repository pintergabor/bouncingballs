package eu.pintergabor.bouncingballs.ui;

import android.os.Bundle;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.*;

import eu.pintergabor.bouncingballs.R;
import eu.pintergabor.bouncingballs.com.MainHub;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;
import eu.pintergabor.util.runcount.RunCount;

/**
 * Main activity.
 */
public class MainActivity extends AppCompatActivity {

    // region // Easy access to MainActivity from anywhere.

    /**
     * Store link to {@link MainActivity}.
     */
    private static MainActivity mainActivity;

    /**
     * Return link to {@link MainActivity}.
     * <p>
     * Valid from onCreate till onDestroy.
     *
     * @return {@link MainActivity}.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    // endregion

    /**
     * Standard pattern.
     *
     * @param savedInstanceState Not used.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Store a link to MainActivity for easy access
        mainActivity = this;
        // Create communication interface
        MainHub.createInstance();
        // Main layout
        setContentView(R.layout.activity_main);
        // Manage visibility of UI elements
        ManageViews.createInstance();
        // Load, and increment, run counter
        RunCount.getInstance().load(this).increment();
    }

    /**
     * Start animation.
     */
    @Override
    protected void onResume() {
        super.onResume();
        ManageViews manageViews = ManageViews.getInstance();
        // Start
        manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
    }

    /**
     * Save state and stop animation.
     */
    @Override
    protected void onPause() {
        super.onPause();
        ManageViews manageViews = ManageViews.getInstance();
        // Stop
        manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.GONE);
        // Save the already incremented run counter
        RunCount.getInstance().save(this);
    }

    /**
     * End.
     */
    @Override
    protected void onDestroy() {
        // Destroy communication interface, and UI
        MainHub.destroyInstance();
        ManageViews.destroyInstance();
        // End
        mainActivity = null;
        super.onDestroy();
    }

    /**
     * Hook {@link ManageViews#onBackPressed()} into back button processing.
     */
    @Override
    public void onBackPressed() {
        ManageViews manageViews = ManageViews.getInstance();
        if (!manageViews.onBackPressed()) {
            super.onBackPressed();
        }
    }

    /**
     * Hook {@link ManageViews#onKeyDown(int, KeyEvent)} into keypress processing.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        ManageViews manageViews = ManageViews.getInstance();
        if (manageViews.onKeyDown(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
