package eu.pintergabor.bouncingballs.ui;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import eu.pintergabor.bouncingballs.R;
import eu.pintergabor.bouncingballs.global.Const;
import eu.pintergabor.util.preferences.ValueToSummary;

/**
 * The core of settings.
 */
public class PrefsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);

        // Bind the summaries of EditText/List/Dialog/etc preferences
        // to their values. When their values change, their summaries are
        // updated to reflect the new value, per the Android Design
        // guidelines.
        ValueToSummary changeListener = new ValueToSummary();
        changeListener.bindPreferenceValueToSummary(findPreference(Const.BALL_TYPE));
        changeListener.bindPreferenceValueToSummary(findPreference(Const.SETTINGS_ACCESS));
    }

}
