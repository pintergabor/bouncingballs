package eu.pintergabor.bouncingballs.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.*;
import androidx.fragment.app.Fragment;

import eu.pintergabor.bouncingballs.BuildConfig;
import eu.pintergabor.bouncingballs.R;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;

/**
 * About.
 */
public class AboutFragment extends Fragment {

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_about, container, false);
        // Set version number
        final TextView version = v.findViewById(R.id.about_version);
        version.setText(BuildConfig.VERSION_NAME);
        // Done button changes view state to SETTINGS
        v.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManageViews manageViews = ManageViews.getInstance();
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS);
            }
        });
        // Done
        return v;
    }

}
