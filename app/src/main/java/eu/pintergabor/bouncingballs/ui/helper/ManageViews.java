package eu.pintergabor.bouncingballs.ui.helper;

import android.os.Build;
import android.preference.PreferenceManager;
import android.transition.TransitionManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.*;

import org.jetbrains.annotations.*;

import eu.pintergabor.bouncingballs.R;
import eu.pintergabor.bouncingballs.global.Const;
import eu.pintergabor.bouncingballs.ui.MainActivity;
import eu.pintergabor.bouncingballs.main.MainView;
import eu.pintergabor.util.preferences.ValueFromPreferences;

/**
 * Manage views displayed in the main application frame.
 */
public class ManageViews {

    // region // Singleton pattern.

    private static ManageViews instance;

    /**
     * Singleton.
     */
    private ManageViews() {
        // Nothing.
    }

    /**
     * Start managing views.
     * <p>
     * Called from MainActivity.onCreate.
     */
    public static void createInstance() {
        instance = new ManageViews();
        // Track display size changes.
        instance.startLayoutChangeListener();
        // Initial view
        instance.refreshCurrentViewstate();
    }

    /**
     * Get instance.
     * <p>
     * The instance is valid from MainActivity.onCreate till MainActivity.onDestroy.
     *
     * @return Singleton.
     */
    @Contract(pure = true)
    public static ManageViews getInstance() {
        return instance;
    }

    /**
     * Destroy instance.
     * <p>
     * Called from MainActivity.onDestroy.
     */
    public static void destroyInstance() {
        instance = null;
    }

    // endregion

    // region // Full screen (full frame) properties.

    private int frameWidth;

    /**
     * Frame width.
     * (= Display width in full screen mode.)
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public int getFrameWidth() {
        return frameWidth;
    }

    private int frameHeight;

    /**
     * Frame height.
     * (= Display height in full screen mode.)
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public int getFrameHeight() {
        return frameHeight;
    }

    private float frameDensity;

    /**
     * Frame resolution.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public float getFrameDensity() {
        return frameDensity;
    }

    /**
     * Keep
     * {@link ManageViews#getFrameWidth()},
     * {@link ManageViews#getFrameHeight()} and
     * {@link ManageViews#getFrameDensity()} up to date.
     */
    private void startLayoutChangeListener() {
        MainActivity mainActivity = MainActivity.getMainActivity();
        frameDensity = mainActivity.getResources().getDisplayMetrics().density;
        View mainView = mainActivity.findViewById(R.id.frame_main);
        ViewGroup.LayoutParams params = mainView.getLayoutParams();
        frameWidth = params.width;
        frameHeight = params.height;
        // Track display size changes
        mainView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            /**
             * Get display size and update
             * {@link ManageViews#getFrameWidth()},
             * {@link ManageViews#getFrameHeight()} and
             * {@link ManageViews#getFrameDensity()}.
             */
            @Override
            public void onLayoutChange(View view,
                                       int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                final int w = right - left;
                final int h = bottom - top;
                if (frameWidth != w || frameHeight != h) {
                    frameWidth = w;
                    frameHeight = h;
                    refreshCurrentViewstate();
                }
            }
        });
    }

    // endregion

    // region // View states.

    /**
     * All possible view states.
     *
     * <table>
     * <tr><td>SETTINGS</td><td> - </td><td>Settings only</td></tr>
     * <tr><td>MAIN</td><td> - </td><td>Main only</td></tr>
     * <tr><td>ABOUT</td><td> - </td><td>About only</td></tr>
     * </table>
     * <p>
     */
    public enum VIEWSTATE {
        GONE,
        SETTINGS,
        MAIN,
        ABOUT,
    }

    /**
     * Current view state.
     */
    private VIEWSTATE currentViewstate = VIEWSTATE.GONE;

    /**
     * Set size of a {@link View}.
     *
     * @param view   The {@link View}.
     * @param width  Desired width in pixels.
     * @param height Desired height in pixels.
     */
    private void setSize(@NonNull View view, int width, int height) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }

    /**
     * Set offset of a {@link View}.
     *
     * @param view   The {@link View}.
     * @param offset Desired offset in pixels.
     */
    private void setOffset(@NonNull View view, int offset) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.leftMargin = offset;
        params.rightMargin = -offset;
        view.setLayoutParams(params);
    }

    /**
     * Set the settings button visibility according to the value set in preferences.
     */
    private void setSettingsButtonVisibility() {
        final MainActivity mainActivity = MainActivity.getMainActivity();
        // Get parameters
        ValueFromPreferences vfp = new ValueFromPreferences(PreferenceManager
            .getDefaultSharedPreferences(mainActivity));
        final int visibility = vfp.getInt(Const.SETTINGS_ACCESS, 0);
        // Set visibility
        final Button button = mainActivity.findViewById(R.id.settings_button);
        if (visibility == 0) {
            button.setVisibility(View.VISIBLE);
        } else {
            button.setVisibility(View.GONE);
        }
    }

    /**
     * Get current view state.
     *
     * @return Current {@link VIEWSTATE}.
     */
    @SuppressWarnings("unused")
    public VIEWSTATE getCurrentViewstate() {
        return currentViewstate;
    }

    /**
     * Change view state.
     *
     * @param v New {@link VIEWSTATE}.
     */
    public void setCurrentViewstate(VIEWSTATE v) {
        final MainActivity mainActivity = MainActivity.getMainActivity();
        // Get references to the embedded views
        final View container = mainActivity.findViewById(R.id.container_main);
        final View settings = mainActivity.findViewById(R.id.fragment_settings);
        final View about = mainActivity.findViewById(R.id.fragment_about);
        final View main = mainActivity.findViewById(R.id.fragment_main);
        final MainView mainView = mainActivity.findViewById(R.id.main_view);
        // Request animation
        if (21 <= Build.VERSION.SDK_INT) {
            ViewGroup fullscreen = mainActivity.findViewById(R.id.frame_main);
            TransitionManager.beginDelayedTransition(fullscreen);
        }
        // Change the size of the embedded views.
        final int w = frameWidth;
        final int h = frameHeight;
        if (0 < w && 0 < h) {
            switch (v) {
                case SETTINGS:
                    mainView.stop();
                    setSize(container, 2 * w, h);
                    setSize(settings, w, h);
                    setSize(about, w, 0);
                    setSize(main, w, h);
                    setOffset(container, 0);
                    break;
                case MAIN:
                    setSettingsButtonVisibility();
                    setSize(container, 2 * w, h);
                    setSize(settings, w, h);
                    setSize(about, w, 0);
                    setSize(main, w, h);
                    setOffset(container, -w);
                    mainView.start();
                    break;
                case ABOUT:
                    mainView.stop();
                    setSize(container, 2 * w, h);
                    setSize(settings, w, 0);
                    setSize(about, w, h);
                    setSize(main, w, h);
                    setOffset(container, 0);
                    break;
                case GONE:
                    mainView.stop();
                    break;
            }
        }
        // Remember the view state.
        currentViewstate = v;
    }

    /**
     * Refresh current view state if frame size or orientation changes.
     */
    @SuppressWarnings("unused")
    public void refreshCurrentViewstate() {
        setCurrentViewstate(currentViewstate);
    }

    /**
     * Shift view state as a response to left to right fling.
     */
    @SuppressWarnings("unused")
    public void prev() {
        if (currentViewstate == VIEWSTATE.MAIN) {
            setCurrentViewstate(VIEWSTATE.SETTINGS);
        }
    }

    /**
     * Shift view state as a response to right to left fling.
     */
    @SuppressWarnings("unused")
    public void next() {
        if (currentViewstate == VIEWSTATE.SETTINGS) {
            setCurrentViewstate(VIEWSTATE.MAIN);
        }
    }

    /**
     * Call from {@link MainActivity} when the back button is pressed.
     */
    public boolean onBackPressed() {
        if (currentViewstate == VIEWSTATE.SETTINGS) {
            setCurrentViewstate(VIEWSTATE.MAIN);
            return true;
        }
        return false;
    }

    /**
     * Call from {@link MainActivity} when a key is is pressed.
     */

    public boolean onKeyDown(int keyCode, @SuppressWarnings("unused") KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_F1:
            case KeyEvent.KEYCODE_S:
                setCurrentViewstate(VIEWSTATE.SETTINGS);
                return true;
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_F2:
            case KeyEvent.KEYCODE_SPACE:
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_W:
            case KeyEvent.KEYCODE_M:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                setCurrentViewstate(VIEWSTATE.MAIN);
                return true;
        }
        return false;
    }

    // endregion

}
