package eu.pintergabor.bouncingballs.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.*;
import androidx.fragment.app.Fragment;

import eu.pintergabor.bouncingballs.R;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;

/**
 * Main view containing the bouncing balls.
 */
public class MainFragment extends Fragment {

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate
        View mainView = inflater.inflate(R.layout.fragment_main, container, false);
        // Settings button changes view state to SETTINGS
        Button settingsButton = mainView.findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManageViews manageViews = ManageViews.getInstance();
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS);
            }
        });
        // Return
        return mainView;
    }

}
