package eu.pintergabor.bouncingballs.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.*;
import androidx.fragment.app.Fragment;

import eu.pintergabor.bouncingballs.R;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;

/**
 * Settings.
 */
public class SettingsFragment extends Fragment {

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_settings, container, false);
        // Apply button changes view state to MAIN
        v.findViewById(R.id.done_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManageViews manageViews = ManageViews.getInstance();
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
            }
        });
        // About button changes view state to ABOUT
        v.findViewById(R.id.help_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManageViews manageViews = ManageViews.getInstance();
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT);
            }
        });
        // Return
        return v;
    }

}
