package eu.pintergabor.bouncingballs.main;

import android.os.SystemClock;

import eu.pintergabor.bouncingballs.balls.BallsMove;
import eu.pintergabor.common.TerminateThread;

/**
 * Calculator thread.
 */
public class Calculator extends TerminateThread {

    @SuppressWarnings("unused")
    static final String TAG = "CALCULATOR";

    // region // Owner view.

    /**
     * The owner.
     */
    private MainView mainView;

    /**
     * Store the owner view of {@link Coordinator},
     * {@link Renderer} and {@link Calculator}.
     *
     * @param mainView The owner.
     */
    public Calculator(MainView mainView) {
        super();
        this.mainView = mainView;
        drawing = new BallsMove();
    }

    // endregion

    /**
     * The balls.
     */
    private BallsMove drawing;

    /**
     * Move the balls continuously, and pass them to {@link Renderer}.
     */
    @Override
    public void run() {
        final Renderer renderer = mainView.getRenderer();
        // Start now.
        long t0 = SystemClock.uptimeMillis();
        while (isRunning()) {
            try {
                // Calculate it.
                final long now = SystemClock.uptimeMillis();
                final int dt = (int) (now - t0);
                if (0 < dt) {
                    drawing.moveBalls(dt);
                    drawing.gravityBalls(dt);
                    drawing.touchBalls(dt);
                    drawing.dragBalls(dt);
                    drawing.driftBalls(dt);
                    t0 = now;
                } else {
                    sleep(1);
                }
                // Pass it to renderer.
                renderer.putInput(drawing);
            } catch (InterruptedException e) {
                // It is OK.
            }
        }
    }

}
