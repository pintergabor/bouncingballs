package eu.pintergabor.bouncingballs.main;

import android.graphics.Bitmap;
import android.os.Handler;

import org.jetbrains.annotations.*;

/**
 * Coordinate UI thread and {@link Calculator} thread.
 * <p>
 * Request the UI to redraw the image
 * after the minimum refresh interval
 * when the new bitmap is available.
 * <p>
 * In response to this UI thread calls {@link #getOutput()} from onDraw
 * and takes the posted bitmap.
 */
public class Coordinator {

    @SuppressWarnings("unused")
    static final String TAG = "COORDINATOR";

    // region // Owner view.

    /**
     * The owner.
     */
    private MainView mainView;

    /**
     * Store the owner view of {@link Coordinator},
     * {@link Renderer} and {@link Calculator}.
     *
     * @param mainView The owner.
     */
    public Coordinator(MainView mainView) {
        this.mainView = mainView;
    }

    // endregion

    // region // Timer

    private final Handler timer = new Handler();

    /**
     * true if {@link #timer} is running.
     */
    private boolean timerRunning;

    /**
     * Minimum screen refresh time.
     */
    private final int TIMEOUT = 10;

    /**
     * When {@link #TIMEOUT} elapsed.
     */
    private Runnable onFinish = new Runnable() {
        @Override
        public void run() {
            timeout();
        }
    };

    // endregion

    // region // Bitmaps

    /**
     * The currently displayed bitmap.
     * <p>
     * Outside {@link Coordinator} only the UI thread is allowed to access it.
     */
    private Bitmap output;

    /**
     * The posted new bitmap.
     */
    private Bitmap waiting;

    /**
     * A bitmap no longer needed and can be recycled.
     */
    private Bitmap recycle;

    // endregion

    // region // Utils.

    /**
     * Ask the UI thread to redraw the image.
     */
    private synchronized void drawRequest() {
        mainView.postInvalidate();
    }

    /**
     * If a new bitmap is waiting by the end of the refresh interval,
     * ask the UI thread to take it over and draw it on screen.
     * <p>
     * If not, then wait for the calculator thread to post a new bitmap,
     * and that will ask the UI thread to take it over and draw it on screen.
     */
    private synchronized void timeout() {
        // If a new bitmap is available
        if (waiting != null) {
            // Ask the UI thread to draw it
            drawRequest();
        }
        timerRunning = false;
    }

    // endregion

    // region // UI side interface.

    /**
     * Get the posted bitmap.
     * <p>
     * Called from the UI thread.
     * <p>
     * After this call UI have the latest bitmap,
     * the waiting bitmap will be empty,
     * and this allows the calculator thread to post the next bitmap.
     */
    public synchronized Bitmap getOutput() {
        // If a new bitmap is available
        if (waiting != null) {
            // Move pointers around
            recycle = output;
            output = waiting;
            waiting = null;
        }
        // Signal to Renderer to create the next bitmap
        final Renderer renderer = mainView.getRenderer();
        renderer.notifyOutput();
        // Now output contains the latest bitmap
        return output;
    }

    // endregion

    // region // Calculator side interface.

    /**
     * Check if the UI thread needs a new bitmap.
     *
     * @return true if the UI thread needs a new bitmap.
     */
    public synchronized boolean needInput() {
        return waiting == null;
    }

    /**
     * Post the newly calculated bitmap to the UI thread.
     * <p>
     * Called from the calculator thread.
     * <p>
     * Return a recycled bitmap, which may be null
     * or the wrong size and may not be empty.
     * <p>
     * If the timer is not running (never started, or already finished),
     * then ask the UI thread to take it over and draw it on screen,
     * and restart the timer for the next update.
     */
    public synchronized Bitmap putInput(@NotNull Bitmap input) {
        // If there is space for the newly calculated bitmap,
        if (waiting == null) {
            // move pointers around.
            waiting = input;
            input = recycle;
            recycle = null;
        }
        // Ask for redraw after the minimum refresh interval.
        if (!timerRunning) {
            drawRequest();
            // Prepare for the next update.
            timer.postDelayed(onFinish, TIMEOUT);
            timerRunning = true;
        }
        // Return what was in recycle.
        return input;
    }

    // endregion

}
