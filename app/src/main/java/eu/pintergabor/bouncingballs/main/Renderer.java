package eu.pintergabor.bouncingballs.main;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import org.jetbrains.annotations.*;

import eu.pintergabor.bouncingballs.balls.BallsDraw;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;
import eu.pintergabor.common.TerminateThread;

/**
 * Coordinate UI thread and {@link Calculator} thread.
 * <p>
 * Render and post a bitmap to {@link Coordinator} when it has space for it.
 * <p>
 * Then take the next set of balls from {@link Calculator} for rendering.
 */
public class Renderer extends TerminateThread {

    @SuppressWarnings("unused")
    static final String TAG = "RENDERER";

    // region // Owner view.

    /**
     * The owner.
     */
    private MainView mainView;

    /**
     * Store the owner view of {@link Coordinator},
     * {@link Renderer} and {@link Calculator}.
     *
     * @param mainView The owner.
     */
    public Renderer(MainView mainView) {
        this.mainView = mainView;
    }

    // endregion

    // region // Balls.

    private Bitmap outputBitmap;

    /**
     * The currently rendered balls.
     */
    private BallsDraw output;

    /**
     * The posted new balls.
     */
    private BallsDraw waiting;

    /**
     * Balls no longer needed and can be recycled.
     */
    private BallsDraw recycle;

    /**
     * Notify when {@link Coordinator} can take a new bitmap.
     */
    private final Object sigOutput = new Object();

    /**
     * Notify when {@link Calculator} passes {@link Renderer}
     * a new set of Balls through {@link #putInput(BallsDraw)}.
     */
    private final Object sigInput = new Object();

    /**
     * Call from {@link Coordinator#getOutput()} to notify
     * that {@link Coordinator} can take a new bitmap.
     */
    public void notifyOutput() {
        synchronized (sigOutput) {
            sigOutput.notifyAll();
        }
    }

    // endregion

    // region // Calculator side interface.

    /**
     * Post the newly balls to renderer.
     * <p>
     * Called from the calculator thread.
     * <p>
     * Return a recycled balls, which may be null
     * or the wrong size and may not be empty.
     * <p>
     * If the timer is not running (never started, or already finished),
     * then ask the UI thread to take it over and draw it on screen,
     * and restart the timer for the next update.
     */
    public void putInput(@NotNull BallsDraw input) {
        synchronized (this) {
            // If there is space for the newly calculated balls,
            if (waiting == null) {
                // Try to use recycle, but create a new one if recycle is empty
                waiting = recycle;
                if (waiting == null) {
                    waiting = new BallsDraw();
                }
                recycle = null;
                // Copy contents from input
                waiting.clone(input);
            }
        }
        synchronized (sigInput) {
            // Signal that Renderer can take another set of Balls
            sigInput.notifyAll();
        }
    }

    // endregion

    /**
     * Move the balls continuously, and create and pass bitmaps
     * to UI when the UI is ready to display them.
     */
    @Override
    public void run() {
        final Coordinator coordinator = mainView.getCoordinator();
        final ManageViews manageViews = ManageViews.getInstance();
        while (isRunning()) {
            try {
                // Wait until the UI needs input
                synchronized (sigOutput) {
                    while (!coordinator.needInput()) {
                        sigOutput.wait();
                    }
                }
                // Wait for input from calculator
                synchronized (sigInput) {
                    while (waiting == null) {
                        sigInput.wait();
                    }
                }
                // Take over for rendering
                synchronized (this) {
                    recycle = output;
                    output = waiting;
                    waiting = null;
                }
                // If outputBitmap is null or the wrong size, create a new one
                final int width = manageViews.getFrameWidth();
                final int height = manageViews.getFrameHeight();
                if (outputBitmap == null ||
                    outputBitmap.getWidth() != width ||
                    outputBitmap.getHeight() != height) {
                    outputBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                }
                // Render
                final Canvas canvas = new Canvas(outputBitmap);
                output.draw(canvas);
                outputBitmap = coordinator.putInput(outputBitmap);
            } catch (InterruptedException e) {
                // It is OK.
            }
        }
    }

}
