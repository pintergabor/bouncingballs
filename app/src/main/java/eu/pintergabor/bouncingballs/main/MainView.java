package eu.pintergabor.bouncingballs.main;

import android.annotation.*;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.*;
import androidx.core.view.GestureDetectorCompat;

import eu.pintergabor.bouncingballs.com.MainHub;
import eu.pintergabor.bouncingballs.ui.MainActivity;
import eu.pintergabor.bouncingballs.ui.helper.ManageViews;
import eu.pintergabor.util.gravity.GravityRecorder;
import eu.pintergabor.util.touch.TouchRecorder;
import eu.pintergabor.util.ui.FlingDetector;

/**
 * Main, fullscreen {@link View} where the action happens.
 */
public class MainView extends View {

    @SuppressWarnings("unused")
    static final String TAG = "MAINVIEW";

    // region // Constructors.

    /**
     * As required + local init.
     */
    public MainView(Context context) {
        super(context);
        init(null);
    }

    /**
     * As required + local init.
     */
    public MainView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    /**
     * As required + local init.
     */
    public MainView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    /**
     * As required + local init.
     */
    @RequiresApi(21)
    @SuppressWarnings("unused")
    public MainView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    // endregion

    // region // Fling detector and touch recorder.

    /**
     * Called at the end of every constructor to initialize custom fields.
     *
     * @param attrs Attribute set.
     */
    private void init(@Nullable @SuppressWarnings("unused") AttributeSet attrs) {
        MainActivity mainActivity = MainActivity.getMainActivity();
        // Fling from left to right -> change view state to SETTINGS
        flingDetector = new FlingDetector();
        flingDetector.setRightFlingListener(rightFlingListener);
        gestureDetector = new GestureDetectorCompat(mainActivity, flingDetector);
    }

    /**
     * Used by {@link #flingDetector}.
     */
    private GestureDetectorCompat gestureDetector;

    /**
     * Detect flings.
     */
    private FlingDetector flingDetector;

    /**
     * Listen to touch events and keep a record of ids and positions of all touch points in {@link TouchRecorder}.
     * Hook {@link #gestureDetector} into touch events.
     *
     * @param event Touch event.
     * @return true.
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        // Record
        TouchRecorder.getInstance().onTouchEvent(event);
        // Hook gestureDetector into touch events.
        gestureDetector.onTouchEvent(event);
        return true;
    }

    /**
     * Call when there is a left to right fling.
     */
    private FlingDetector.RightFlingListener rightFlingListener = new FlingDetector.RightFlingListener() {
        @Override
        public boolean onRightFling() {
            final ManageViews manageViews = ManageViews.getInstance();
            manageViews.prev();
            return true;
        }
    };

    // endregion

    // region // Coordinator, Renderer and Calculator.

    private Coordinator coordinator;

    /**
     * Coordinate UI and {@link Renderer} thread.
     */
    @SuppressWarnings("unused")
    public Coordinator getCoordinator() {
        return coordinator;
    }

    private Renderer renderer;

    /**
     * Coordinate {@link Renderer} and {@link Calculator} thread.
     */
    @SuppressWarnings("unused")
    public Renderer getRenderer() {
        return renderer;
    }

    private Calculator calculator;

    /**
     * Calculate and post new bitmaps.
     */
    @SuppressWarnings("unused")
    public Calculator getCalculator() {
        return calculator;
    }

    /**
     * Start animation and enable the gravity sensor.
     */
    public void start() {
        if (calculator == null) {
            // Enable the gravity sensor
            GravityRecorder.createInstance(getContext());
            // Update parameters from Preferences
            final MainHub mainHub = MainHub.getInstance();
            mainHub.updateParams();
            // Create and initialize ball images
            mainHub.createBalls();
            // Create coordinator, renderer and calculator
            coordinator = new Coordinator(this);
            renderer = new Renderer(this);
            calculator = new Calculator(this);
            // Start
            renderer.start();
            calculator.start();
        }
    }

    /**
     * Stop animation and disable the gravity sensor.
     */
    public void stop() {
        // Stop moving the balls
        // by destroying calculator, renderer and coordinator
        if (calculator != null) {
            calculator.terminateRun();
            calculator = null;
        }
        if (renderer != null) {
            renderer.terminateRun();
            renderer = null;
        }
        if (coordinator != null) {
            coordinator = null;
        }
        // Disable the gravity sensor
        GravityRecorder.destroyInstance();
    }

    // endregion

    // region // Draw.

    /**
     * Draw the same or the next bitmap.
     *
     * @param canvas On this canvas.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (coordinator != null) {
            final Bitmap disp = coordinator.getOutput();
            // Draw the bitmap
            if (disp != null) {
                canvas.drawBitmap(disp, 0, 0, null);
            }
        }
    }

    // endregion

}
