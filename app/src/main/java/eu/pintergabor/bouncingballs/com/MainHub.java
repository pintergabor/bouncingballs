package eu.pintergabor.bouncingballs.com;

import android.preference.PreferenceManager;

import eu.pintergabor.bouncingballs.ball.ballimage.BallImageInterface;
import eu.pintergabor.bouncingballs.balls.images.RandomBallImages;
import eu.pintergabor.bouncingballs.global.Const;
import eu.pintergabor.bouncingballs.main.MainView;
import eu.pintergabor.bouncingballs.ui.MainActivity;
import eu.pintergabor.bouncingballs.ui.PrefsFragment;
import eu.pintergabor.util.preferences.ValueFromPreferences;

/**
 * Communication interface between {@link PrefsFragment} and {@link MainView}.
 */
public class MainHub {

    // region // Singleton pattern.

    private static MainHub instance;

    /**
     * Singleton.
     */
    private MainHub() {
        // Nothing
    }

    /**
     * Create communication interface.
     * <p>
     * Called from MainActivity.onCreate.
     */
    public static void createInstance() {
        instance = new MainHub();
    }

    /**
     * Get instance.
     * <p>
     * The instance is valid from MainActivity.onCreate till MainActivity.onDestroy.
     *
     * @return Singleton.
     */
    public static MainHub getInstance() {
        return instance;
    }

    /**
     * Destroy communication interface.
     * <p>
     * Called from MainActivity.onDestroy.
     */
    public static void destroyInstance() {
        instance = null;
    }

    // endregion

    // region // Parameters that affect the drawing.

    /**
     * Ball image type.
     */
    public int nBallType = Const.BALL_IMAGE_FANCY;

    /**
     * Ball size in dp.
     */
    public int nBallSize = 50;

    /**
     * Number of different ball images.
     */
    public int nBallImages = 10;

    /**
     * The number of balls.
     */
    public int nBalls = 100;

    /**
     * Ball's initial max speed in dp/s.
     */
    public float nBallSpeed = 500;

    /**
     * Drag in dp/s/s.
     */
    public float nDrag = 100;

    /**
     * Max drift in dp/s/s.
     */
    public float nDrift = 50;

    /**
     * Effect of gravity.
     * <p>
     * [-1.0 .. +1.0]
     */
    public float nGravity;

    /**
     * Effect of touch points.
     * <p>
     * [-1.0 .. +1.0]
     */
    public float nTouch;

    /**
     * Update parameters from Preferences.
     */
    public void updateParams() {
        final MainActivity mainActivity = MainActivity.getMainActivity();
        // Get parameters
        ValueFromPreferences vfp = new ValueFromPreferences(PreferenceManager
            .getDefaultSharedPreferences(mainActivity));
        nBallType = vfp.getInt(Const.BALL_TYPE, nBallType);
        nBallSize = vfp.getInt(Const.BALL_SIZE, nBallSize);
        nBallImages = vfp.getInt(Const.BALL_IMAGES, nBallImages);
        nBalls = vfp.getInt(Const.BALLS, nBalls);
        nBallSpeed = vfp.getFloat(Const.BALL_SPEED, nBallSpeed);
        nDrag = vfp.getFloat(Const.DRAG, nDrag);
        nDrift = vfp.getFloat(Const.DRIFT, nDrift);
        nGravity = vfp.getFloat(Const.GRAVITY, nGravity);
        nTouch = vfp.getFloat(Const.TOUCH, nTouch);
    }

    // endregion

    // region // Ball images.

    /**
     * The image of ball i is ballsToImages[i].
     */
    public BallImageInterface[] ballsToImages;

    /**
     * Create ball images and connect the balls to these images.
     */
    public void createBalls() {
        // Create random ball images
        final RandomBallImages ballImages =
            new RandomBallImages(nBallImages, nBallSize, nBallType);
        // Connect balls to images
        ballsToImages = new BallImageInterface[nBalls];
        for (int i = 0; i < nBalls; i++) {
            // Connect the ball to the image.
            ballsToImages[i] = ballImages.getBallImage(i % nBallImages);
        }
    }

    // endregion

}
