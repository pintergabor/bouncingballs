package eu.pintergabor.bouncingballs.ball.ballimage;

import android.graphics.Canvas;

/**
 * The most basic implementation of {@link BallImageInterface},
 * without {@link BallImageInterface#draw(Canvas, int, int)}.
 */
abstract class BallImage implements BallImageInterface {

    /**
     * Width of the ball in px.
     */
    private int width;

    /**
     * Get the width of the ball.
     *
     * @return The width of the ball in px.
     */
    @Override
    public int getWidth() {
        return width;
    }

    /**
     * Set the width of the ball.
     *
     * @param width The width of the ball in px.
     */
    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Height of the ball in px.
     */
    private int height;

    /**
     * Get the height of the ball.
     *
     * @return The height of the ball in px.
     */
    @Override
    public int getHeight() {
        return height;
    }

    /**
     * Set the height of the ball.
     *
     * @param height The height of the ball in px.
     */
    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public abstract void draw(Canvas canvas, int x, int y);

}
