package eu.pintergabor.bouncingballs.ball.ball;

import eu.pintergabor.common.HasSpeedF;

/**
 * A ball that has size, position and speed.
 * <p>
 * {@link Ball} > {@link SpeedBall} > {@link PositionedBall} > {@link BasicBall}
 */
public class SpeedBall extends PositionedBall implements HasSpeedF {

    /**
     * X speed in mpx/s.
     */
    float xSpeed;

    /**
     * Y speed in mpx/s.
     */
    float ySpeed;

    /**
     * Get the X speed of something.
     *
     * @return The X speed of something.
     */
    public float getXSpeed() {
        return this.xSpeed;
    }

    /**
     * Set the X speed of something.
     *
     * @param xSpeed The X speed of something.
     */
    public void setXSpeed(float xSpeed) {
        this.xSpeed = xSpeed;
    }

    /**
     * Get the Y speed of something.
     *
     * @return The Y speed of something.
     */
    public float getYSpeed() {
        return ySpeed;
    }

    /**
     * Set the Y speed of something.
     *
     * @param ySpeed The Y speed of something.
     */
    public void setYSpeed(float ySpeed) {
        this.ySpeed = ySpeed;
    }

    /**
     * Make a deep copy.
     *
     * @param ball Source to copy from.
     */
    public void clone(SpeedBall ball) {
        super.clone(ball);
        xSpeed = ball.xSpeed;
        ySpeed = ball.ySpeed;
    }

    /**
     * Create empty.
     */
    public SpeedBall() {
        super();
    }

    /**
     * Create a clone.
     *
     * @param ball Source to clone from.
     */
    @SuppressWarnings("unused")
    public SpeedBall(SpeedBall ball) {
        clone(ball);
    }

}
