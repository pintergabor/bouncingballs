package eu.pintergabor.bouncingballs.ball.ball;

import eu.pintergabor.common.HasSize;

/**
 * A basic ball, that has a size.
 * <p>
 * {@link Ball} > {@link SpeedBall} > {@link PositionedBall} > {@link BasicBall}
 */
public class BasicBall implements HasSize {

    /**
     * Width of the ball in px.
     */
    int width;

    /**
     * Get the width of the ball.
     *
     * @return The width of the ball in px.
     */
    @Override
    public int getWidth() {
        return width;
    }

    /**
     * Set the width of the ball.
     *
     * @param width The width of the ball in px.
     */
    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Height of the ball in px.
     */
    int height;

    /**
     * Get the height of the ball.
     *
     * @return The height of the ball in px.
     */
    @Override
    public int getHeight() {
        return height;
    }

    /**
     * Set the height of the ball.
     *
     * @param height The height of the ball in px.
     */
    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Make a deep copy.
     *
     * @param ball Source to copy from.
     */
    public void clone(BasicBall ball) {
        width = ball.width;
        height = ball.height;
    }

    /**
     * Create empty.
     */
    public BasicBall() {
        // Nothing.
    }

    /**
     * Create a clone.
     *
     * @param ball Source to clone.
     */
    @SuppressWarnings("unused")
    public BasicBall(BasicBall ball) {
        clone(ball);
    }

}
