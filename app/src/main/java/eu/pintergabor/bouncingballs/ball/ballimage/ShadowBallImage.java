package eu.pintergabor.bouncingballs.ball.ballimage;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * A pre-rendered version of any other {@link BallImageInterface}.
 * <p>
 * It takes longer to create, and uses more memory, but it is faster to draw.
 */
public class ShadowBallImage implements BallImageInterface {

    /**
     * The original {@link BallImageInterface}.
     */
    private final BallImageInterface original;

    /**
     * A shadow copy of the original {@link BallImageInterface}.
     */
    private Bitmap bitmap;

    /**
     * Create a shadow copy of the original {@link BallImageInterface}.
     *
     * @param original Original {@link BallImageInterface}.
     */
    public ShadowBallImage(BallImageInterface original) {
        this.original = original;
        preRender();
    }

    /**
     * Create pre-rendered bitmap of the original ball image.
     */
    private void preRender() {
        final int width = getWidth();
        final int height = getHeight();
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        original.draw(canvas, width / 2, height / 2);
    }

    /**
     * Get the width of the ball.
     *
     * @return The width of the ball in px.
     */
    @Override
    public int getWidth() {
        return original.getWidth();
    }

    /**
     * Set the width of the ball.
     * <p>
     * Inefficient. Do not use.
     *
     * @param width The width of the ball in px.
     */
    @Override
    public void setWidth(int width) {
        original.setWidth(width);
        preRender();
    }

    /**
     * Get the height of the ball.
     *
     * @return The height of the ball in px.
     */
    @Override
    public int getHeight() {
        return original.getHeight();
    }

    /**
     * Set the height of the ball.
     * <p>
     * Inefficient. Do not use.
     *
     * @param height The height of the ball in px.
     */
    @Override
    public void setHeight(int height) {
        original.setHeight(height);
        preRender();
    }

    @Override
    public void draw(Canvas canvas, int x, int y) {
        canvas.drawBitmap(bitmap, x - getWidth() / 2, y - getHeight() / 2, null);
    }

}
