package eu.pintergabor.bouncingballs.ball.ball;

/**
 * A ball, that has a defined position and (possibly) some speed and
 * may have some other properties in the future.
 *
 * {@link Ball} > {@link SpeedBall} > {@link PositionedBall} > {@link BasicBall}
 */
public class Ball extends SpeedBall {

    /**
     * Make a deep copy.
     *
     * @param ball Source to copy from.
     */
    public void clone(Ball ball) {
        super.clone(ball);
    }

    /**
     * Create empty.
     */
    public Ball() {
        super();
    }

    /**
     * Create a clone.
     *
     * @param ball Source to clone from.
     */
    @SuppressWarnings("unused")
    public Ball(Ball ball) {
        clone(ball);
    }

}
