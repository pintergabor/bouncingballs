package eu.pintergabor.bouncingballs.ball.ball;

import eu.pintergabor.common.HasPositionF;

/**
 * A ball that has a size and also has a position.
 * <p>
 * {@link Ball} > {@link SpeedBall} > {@link PositionedBall} > {@link BasicBall}
 */
class PositionedBall extends BasicBall implements HasPositionF {

    /**
     * X position of the center of the ball in px.
     */
    float x;

    /**
     * Y position of the center of the ball in px.
     */
    float y;

    /**
     * Get the X position of the center of the ball in px.
     *
     * @return The X position of the center of the ball in px.
     */
    public float getX() {
        return this.x;
    }

    /**
     * Set the X position of the center of the ball in px.
     *
     * @param x The X position of the center of the ball in px.
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Get the Y position of the center of the ball in px.
     *
     * @return The Y position of the center of the ball in px.
     */
    public float getY() {
        return this.y;
    }

    /**
     * Set the Y position of the center of the ball in px.
     *
     * @param y The Y position of the center of the ball in px.
     */
    public void setY(float y) {
        this.y = y;
    }


    /**
     * Make a deep copy.
     *
     * @param ball Source to copy from.
     */
    public void clone(PositionedBall ball) {
        super.clone(ball);
        x = ball.x;
        y = ball.y;
    }

    /**
     * Create empty.
     */
    public PositionedBall() {
        super();
    }

    /**
     * Create a clone.
     *
     * @param ball Source to clone from.
     */
    @SuppressWarnings("unused")
    public PositionedBall(PositionedBall ball) {
        clone(ball);
    }

}
