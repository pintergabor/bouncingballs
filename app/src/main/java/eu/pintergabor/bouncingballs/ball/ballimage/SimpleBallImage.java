package eu.pintergabor.bouncingballs.ball.ballimage;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import androidx.annotation.*;

/**
 * A simple ball, that knows how to draw itself.
 */
@SuppressWarnings("unused")
public class SimpleBallImage extends BallImage {

    /**
     * Create a ball.
     *
     * @param density Display density.
     * @param size    Size in dp.
     * @param hue     Hue as [0.0 .. 360.0] degree on the color palette.
     * @param sat     Saturation [0.0 .. 1.0].
     */
    @SuppressWarnings("unused")
    public SimpleBallImage(float density, int size, float hue, float sat) {
        int color = Color.HSVToColor(0xFF, new float[]{hue, sat, 1f});
        paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        // Convert dp to px
        size = Math.round(size * density);
        // Set size
        setWidth(size);
        setHeight(size);
    }

    /**
     * Used by {@link #draw(Canvas, int, int)}.
     */
    private Paint paint;

    /**
     * Draw the ball on the canvas.
     *
     * @param canvas Canvas to draw on.
     * @param x      X coordinate of the center of the ball in px.
     * @param y      Y coordinate of the center of the ball in px.
     */
    @Override
    public void draw(@NonNull Canvas canvas, int x, int y) {
        canvas.drawCircle(x, y, getWidth() / 2f, paint);
    }

}
