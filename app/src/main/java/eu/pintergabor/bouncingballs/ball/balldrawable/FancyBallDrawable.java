package eu.pintergabor.bouncingballs.ball.balldrawable;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

/**
 * A fancy ball.
 */
public class FancyBallDrawable extends GradientDrawable {

    /**
     * Create a fancy ball.
     *
     * @param density   Display density.
     * @param size      Ball size in dp.
     * @param hue       Hue [0..360].
     * @param centersat Saturation at the center [0..1].
     * @param edgesat   Saturation at the edge [0..1].
     */
    public FancyBallDrawable(float density, int size, float hue, float centersat, float edgesat) {
        super(Orientation.LEFT_RIGHT, new int[]{
            Color.HSVToColor(0xFF, new float[]{hue, centersat, 1f}),
            Color.HSVToColor(0xFF, new float[]{hue, edgesat, 1f})});
        // Convert dp to px
        size = Math.round(size * density);
        // Define shape
        setShape(GradientDrawable.OVAL);
        setGradientType(GradientDrawable.RADIAL_GRADIENT);
        setGradientRadius(size / 2f);
        setGradientCenter(0.5f, 0.5f);
        setBounds(0, 0, size, size);
    }

}
