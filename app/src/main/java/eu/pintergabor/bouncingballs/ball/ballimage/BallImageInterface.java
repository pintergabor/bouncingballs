package eu.pintergabor.bouncingballs.ball.ballimage;

import android.graphics.Canvas;

import eu.pintergabor.common.HasSize;

/**
 * A ball, of any type, must have a size and must be drawable on a canvas.
 */
public interface BallImageInterface extends HasSize {

    /**
     * Draw the ball on the canvas.
     *
     * @param canvas Canvas to draw on.
     * @param x      X coordinate of the center of the ball in px.
     * @param y      Y coordinate of the center of the ball in px.
     */
    void draw(Canvas canvas, int x, int y);

}
