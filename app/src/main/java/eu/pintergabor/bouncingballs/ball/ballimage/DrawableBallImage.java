package eu.pintergabor.bouncingballs.ball.ballimage;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;

/**
 * A ball, that has a shape and size and created from a {@link Drawable}.
 * <p>
 * Unlike {@link SimpleBallImage}, it does not know how to draw itself.
 * It relies on the {@link Drawable} for the actual drawing, so the {@link Drawable}
 * must be set either in {@link #DrawableBallImage(Drawable)} constructor, or in
 * {@link #setDrawable(Drawable, int, int)}, or in {@link #setDrawable(Drawable)},
 * before calling {@link #draw(Canvas, int, int)}.
 */
public class DrawableBallImage extends BallImage {

    /**
     * Create a ball without image.
     */
    @SuppressWarnings("unused")
    public DrawableBallImage() {
        this(null);
    }

    /**
     * Create a ball from a {@link Drawable}.
     *
     * @param drawable See {@link #setDrawable(Drawable)}.
     */
    @SuppressWarnings("unused")
    public DrawableBallImage(@Nullable Drawable drawable) {
        this.setDrawable(drawable);
    }

    /**
     * Visual representation of the ball.
     */
    private Drawable drawable;

    /**
     * Get the drawable of the ball.
     *
     * @return Image of the ball.
     */
    @SuppressWarnings("unused")
    public Drawable getDrawable() {
        return drawable;
    }

    /**
     * Define/redefine the drawable and size of the ball.
     *
     * @param drawable Any {@link Drawable} that has a fixed size.
     */
    @SuppressWarnings("unused")
    public void setDrawable(Drawable drawable) {
        setDrawable(drawable, 0, 0);
    }

    /**
     * Define/redefine the drawable and size of the ball.
     *
     * @param drawable Any {@link Drawable} that has a fixed size.
     * @param width    Width of the drawable. If it is zero, use {@link Drawable#getIntrinsicWidth()}.
     * @param height   Height of the drawable. If it is zero, use {@link Drawable#getIntrinsicHeight()}.
     */
    @SuppressWarnings("unused")
    public void setDrawable(Drawable drawable, int width, int height) {
        if (drawable != null) {
            if (width <= 0) {
                if (drawable.getBounds().width() <= 0) {
                    width = drawable.getIntrinsicWidth();
                }
            }
            if (height <= 0) {
                if (drawable.getBounds().height() <= 0) {
                    height = drawable.getIntrinsicHeight();
                }
            }
            if (width > 0 && height > 0) {
                drawable.setBounds(0, 0, width, height);
            }
            // Setting drawable, width and height is an atomic operation
            synchronized (this) {
                this.drawable = drawable;
                this.setWidth(drawable.getBounds().width());
                this.setHeight(drawable.getBounds().height());
            }
        }
    }

    /**
     * Draw the ball on the canvas.
     *
     * @param canvas Canvas to draw on.
     * @param x      X coordinate of the center of the ball in px.
     * @param y      Y coordinate of the center of the ball in px.
     */
    @Override
    public void draw(Canvas canvas, int x, int y) {
        int left;
        int top;
        int width;
        int height;
        // Make access to width and height an atomic operation
        synchronized (this) {
            width = getWidth();
            height = getHeight();
        }
        left = x - width / 2;
        top = y - height / 2;
        if (drawable != null && 0 < width && 0 < height) {
            // Set the position and the size of the ball on the canvas
            drawable.setBounds(left, top, left + width, top + height);
            // Ask the drawable to draw itself
            drawable.draw(canvas);
        }
    }

}
