package eu.pintergabor.bouncingballs.global;

/**
 * Global constants.
 */
public final class Const {

    /**
     * It is a static class.
     */
    private Const() {
        // Nothing.
    }

    // region // Key names for settings.

    /**
     * Settings: Settings access method.
     */
    public static final String SETTINGS_ACCESS = "settings_access";

    /**
     * Settings: Ball type.
     */
    public static final String BALL_TYPE = "ball_type";

    /**
     * Settings: Ball type: Simple.
     */
    public static final int BALL_IMAGE_SIMPLE = 0;

    /**
     * Settings: Ball type: Fancy.
     */
    public static final int BALL_IMAGE_FANCY = 1;

    /**
     * Settings: Ball size in dp.
     */
    public static final String BALL_SIZE = "ball_size";

    /**
     * Settings: Number of ball images.
     */
    public static final String BALL_IMAGES = "ball_images";

    /**
     * Settings: Number of balls.
     */
    public static final String BALLS = "balls";

    /**
     * Settings: Maximum initial speed in dp/s.
     */
    public static final String BALL_SPEED = "ball_speed";

    /**
     * Settings: Drag.
     */
    public static final String DRAG = "drag";

    /**
     * Settings: Drift (aka random movement).
     */
    public static final String DRIFT = "drift";

    /**
     * Settings: Gravity.
     */
    public static final String GRAVITY = "gravity";

    /**
     * Settings: Touch affinity.
     */
    public static final String TOUCH = "touch";

    // endregion

    // region // Other constants.

    /**
     * If gravity sensor value is less than this it is ignored.
     */
    public static final float GLIMIT = 1.0f;

    // endregion

}
