package eu.pintergabor.util.preferences;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

/**
 * Get value from preferences without throwing exception
 */
@SuppressWarnings("unused")
public class ValueFromPreferences {

    /**
     * Shared preferences is stored here.
     */
    @NonNull
    private SharedPreferences pref;

    /**
     * Save preferences for later use.
     *
     * @param pref Preferences.
     */
    @SuppressWarnings("unused")
    public ValueFromPreferences(@NonNull SharedPreferences pref) {
        this.pref = pref;
    }

    /**
     * Get integer, even if the preference was saved as a string.
     *
     * @param key      Key.
     * @param defValue Default value.
     * @return the value of the preference.
     */
    @SuppressWarnings({"unused", "ConstantConditions"})
    public int getInt(String key, int defValue) {
        int i;
        try {
            i = pref.getInt(key, defValue);
        } catch (ClassCastException e1) {
            try {
                i = Integer.parseInt(pref.getString(key, Integer.toString(defValue)));
            } catch (ClassCastException | NumberFormatException | NullPointerException e2) {
                i = defValue;
            }
        }
        return i;
    }

    /**
     * Get float, even if the preference was saved as a string or as an integer.
     *
     * @param key      Key.
     * @param defValue Default value.
     * @return the value of the preference.
     */
    @SuppressWarnings({"unused", "ConstantConditions"})
    public float getFloat(String key, float defValue) {
        float i;
        try {
            i = pref.getFloat(key, defValue);
        } catch (ClassCastException e1) {
            try {
                i = pref.getInt(key, (int) defValue);
            } catch (ClassCastException | NumberFormatException e2) {
                try {
                    i = Float.parseFloat(pref.getString(key, Float.toString(defValue)));
                } catch (ClassCastException | NumberFormatException | NullPointerException e3) {
                    i = defValue;
                }
            }
        }
        return i;
    }
}
