package eu.pintergabor.util.gravity;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Display;
import android.view.WindowManager;

import org.jetbrains.annotations.*;

import static android.view.Surface.ROTATION_0;
import static android.view.Surface.ROTATION_180;
import static android.view.Surface.ROTATION_270;
import static android.view.Surface.ROTATION_90;

public class GravityRecorder implements SensorEventListener {

    // region // Modified singleton pattern.

    private static GravityRecorder instance;

    /**
     * Prepare to monitor the gravity sensor.
     *
     * @param context some context to access {@link Activity#getSystemService}
     */
    private GravityRecorder(Context context) {
        display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        gSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, gSensor, SensorManager.SENSOR_DELAY_UI);
    }

    /**
     * Start listening to gravity sensor.
     */
    public static void createInstance(Context context) {
        instance = new GravityRecorder(context);
    }

    /**
     * Get instance.
     *
     * @return Singleton.
     */
    @Contract(pure = true)
    public static GravityRecorder getInstance() {
        return instance;
    }

    /**
     * Start listening to gravity sensor and destroy instance.
     */
    public static void destroyInstance() {
        if (instance != null) {
            instance.sensorManager.unregisterListener(instance);
            instance = null;
        }
    }

    // endregion

    // region // Record gravity.

    /**
     * To access sensors.
     */
    private SensorManager sensorManager;

    /**
     * To access the accelerometer sensor.
     */
    private Sensor gSensor;

    /**
     * To get the orientation of the display.
     */
    private Display display;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Nothing.
    }

    /**
     * Record the value of gravity.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            synchronized (this) {
                switch (display.getRotation()) {
                    case ROTATION_0:
                        gX = -event.values[0];
                        gY = event.values[1];
                        break;
                    case ROTATION_90:
                        gX = event.values[1];
                        gY = event.values[0];
                        break;
                    case ROTATION_180:
                        gX = -event.values[0];
                        gY = -event.values[1];
                        break;
                    case ROTATION_270:
                        gX = -event.values[1];
                        gY = -event.values[0];
                        break;
                }
            }
        }
    }

    // endregion

    // region // Access the recorded gravity.

    private float gX;

    /**
     * Gravity X direction.
     */
    public float getGX() {
        return gX;
    }

    private float gY;

    /**
     * Gravity Y direction.
     */
    public float getGY() {
        return gY;
    }

    // endregion

}
