package eu.pintergabor.util.ui;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Detect flings.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class FlingDetector extends GestureDetector.SimpleOnGestureListener {

    private static final String TAG = "FLINGDETECTOR";

    /**
     * Every gesture starts with a touch down.
     *
     * @param event Unused.
     * @return true.
     */
    @Override
    public boolean onDown(MotionEvent event) {
        return true;
    }

    /**
     * All fling gesture events.
     *
     * @param event1    Start.
     * @param event2    End.
     * @param velocityX Unused.
     * @param velocityY Unused.
     * @return true if the fling was consumed.
     */
    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        final float dx = event2.getX() - event1.getX();
        final float dy = event2.getY() - event1.getY();
        boolean ret = false;
        if (Math.abs(dx) < Math.abs(dy)) {
            if (0 < dy) {
                // Top to bottom
                Log.d(TAG, "Top to bottom");
                if (topFlingListener != null) {
                    ret = topFlingListener.onTopFling();
                }
            } else {
                // Bottom to top
                Log.d(TAG, "Bottom to top");
                if (bottomFlingListener != null) {
                    ret = bottomFlingListener.onBottomFling();
                }
            }
        } else {
            if (0 < dx) {
                // Left to right
                Log.d(TAG, "Left to right");
                if (rightFlingListener != null) {
                    ret = rightFlingListener.onRightFling();
                }
            } else {
                // Right to left
                Log.d(TAG, "Right to left");
                if (leftFlingListener != null) {
                    ret = leftFlingListener.onLeftFling();
                }
            }
        }
        return ret;
    }

    // region // Right to left

    private LeftFlingListener leftFlingListener;

    /**
     * Set callback on fling from right to left.
     *
     * @param listener Callback.
     */
    public void setLeftFlingListener(LeftFlingListener listener) {
        leftFlingListener = listener;
    }

    /**
     * Callback on fling from right to left.
     */
    public interface LeftFlingListener {
        /**
         * Called when a right to left fling occurs.
         *
         * @return true if it consumed the fling.
         */
        boolean onLeftFling();
    }

    // endregion
    // region // Left to right

    private RightFlingListener rightFlingListener;

    /**
     * Set callback on fling from left to right.
     *
     * @param listener Callback.
     */
    public void setRightFlingListener(RightFlingListener listener) {
        rightFlingListener = listener;
    }

    /**
     * Callback on fling from left to right.
     */
    public interface RightFlingListener {
        /**
         * Called when a left to right fling occurs.
         *
         * @return true if it consumed the fling.
         */
        boolean onRightFling();
    }

    // endregion
    // region // Bottom to top

    private TopFlingListener topFlingListener;

    /**
     * Set callback on fling from bottom to top.
     *
     * @param listener Callback.
     */
    public void setTopFlingListener(TopFlingListener listener) {
        topFlingListener = listener;
    }

    /**
     * Callback on fling from bottom to top.
     */
    public interface TopFlingListener {
        /**
         * Called when a bottom to top fling occurs.
         *
         * @return true if it consumed the fling.
         */
        boolean onTopFling();
    }

    // endregion
    // region // Top to bottom

    private BottomFlingListener bottomFlingListener;

    /**
     * Set callback on fling from top to bottom.
     *
     * @param listener Callback.
     */
    public void setBottomFlingListener(BottomFlingListener listener) {
        bottomFlingListener = listener;
    }

    /**
     * Callback on fling from top to bottom.
     */
    public interface BottomFlingListener {
        /**
         * Called when a top to bottom fling occurs.
         *
         * @return true if it consumed the fling.
         */
        boolean onBottomFling();
    }

    // endregion
}
