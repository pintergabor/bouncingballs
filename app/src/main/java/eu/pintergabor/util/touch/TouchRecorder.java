package eu.pintergabor.util.touch;

import android.annotation.SuppressLint;
import android.util.SparseArray;
import android.view.MotionEvent;

/**
 * Keep track of touch pointers across events.
 */
@SuppressWarnings("unused")
public class TouchRecorder {

    // region // Eager singleton pattern.

    private static TouchRecorder instance = new TouchRecorder();

    /**
     * Get instance.
     *
     * @return Singleton.
     */
    public static TouchRecorder getInstance() {
        return instance;
    }

    // endregion

    /**
     * IDs and positions of all active {@link TouchPoint}s.
     */
    public SparseArray<TouchPoint> touches;

    private TouchRecorder() {
        touches = new SparseArray<>(10);
    }

    /**
     * Called when a touch starts.
     *
     * @param id       ID of the touch point.
     * @param x        X coordinate.
     * @param y        Y coordinate.
     * @param pressure Pressure.
     */
    private void touchDown(int id, float x, float y, float pressure) {
        synchronized (this) {
            // Create
            TouchPoint data = new TouchPoint();
            // Set
            data.setTouch(x, y, pressure);
            data.id = id;
            // Remember
            touches.put(id, data);
        }
    }

    /**
     * Called when an existing touch point changes.
     *
     * @param id       ID of the touch point.
     * @param x        X coordinate.
     * @param y        Y coordinate.
     * @param pressure Pressure.
     */
    private void touchMove(int id, float x, float y, float pressure) {
        synchronized (this) {
            // Retrieve
            TouchPoint data = touches.get(id);
            // or create, if not existed
            if (data == null) {
                data = new TouchPoint();
                data.id = id;
            }
            // Modify
            data.setTouch(x, y, pressure);
        }
    }

    /**
     * Called when a touch ends.
     *
     * @param id       ID of the touch point.
     * @param x        X coordinate.
     * @param y        Y coordinate.
     * @param pressure Pressure.
     */
    private void touchUp(int id, float x, float y, float pressure) {
        synchronized (this) {
            // Retrieve
            TouchPoint data = touches.get(id);
            // Forget
            if (data != null) {
                touches.remove(id);
            }
        }
    }

    /**
     * Called when something unexpected happens.
     */
    private void touchCancel() {
        synchronized (this) {
            // Forget all
            touches.clear();
        }
    }

    /**
     * Listen to touch events and keep a record of ids and positions of all touch points in {@link #touches}.
     *
     * @param event Touch event.
     * @return true, if processed the event.
     */
    @SuppressLint("ClickableViewAccessibility")
    @SuppressWarnings({"UnusedReturnValue", "unused"})
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        int index;
        int id;

        /*
         * Switch on the action. The action is extracted from the event by
         * applying the MotionEvent.ACTION_MASK. Alternatively a call to
         * event.getActionMasked() would yield in the action as well.
         */
        switch (action & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                /*
                 * The primary pointer has gone down.
                 */

                /*
                 * Only one touch event is stored in the MotionEvent. Extract
                 * the pointer identifier of this touch from the first index
                 * within the MotionEvent object.
                 */
                id = event.getPointerId(0);
                /*
                 * Store the data under its pointer identifier. The pointer
                 * number stays consistent for the duration of a gesture,
                 * accounting for other pointers going up or down.
                 */
                touchDown(id, event.getX(0), event.getY(0), event.getPressure(0));
                return true;

            case MotionEvent.ACTION_POINTER_DOWN:
                /*
                 * A non-primary pointer has gone down, after an event for the
                 * primary pointer (ACTION_DOWN) has already been received.
                 */

                /*
                 * The MotionEvent object contains multiple pointers. Need to
                 * extract the index at which the data for this particular event
                 * is stored.
                 */
                index = event.getActionIndex();
                id = event.getPointerId(index);
                /*
                 * Store the data under its pointer identifier. The index of
                 * this pointer can change over multiple events, but this
                 * pointer is always identified by the same identifier for this
                 * active gesture.
                 */
                touchDown(id, event.getX(index), event.getY(index), event.getPressure(index));
                return true;

            case MotionEvent.ACTION_UP:
                /*
                 * Final pointer has gone up and has ended the last pressed
                 * gesture.
                 */

                /*
                 * Extract the pointer identifier for the only event stored in
                 * the MotionEvent object.
                 */
                id = event.getPointerId(0);
                /*
                 * And remove it from the list of active touches.
                 */
                touchUp(id, event.getX(0), event.getY(0), event.getPressure(0));
                return true;

            case MotionEvent.ACTION_POINTER_UP:
                /*
                 * A non-primary pointer has gone up and other pointers are
                 * still active.
                 */

                /*
                 * The MotionEvent object contains multiple pointers. Need to
                 * extract the index at which the data for this particular event
                 * is stored.
                 */
                index = event.getActionIndex();
                id = event.getPointerId(index);
                /*
                 * And remove it from the list of active touches.
                 */
                touchUp(id, event.getX(index), event.getY(index), event.getPressure(index));
                return true;

            case MotionEvent.ACTION_MOVE:
                /*
                 * A change event happened during a pressed gesture. (Between
                 * ACTION_DOWN and ACTION_UP or ACTION_POINTER_DOWN and
                 * ACTION_POINTER_UP)
                 */

                /*
                 * Loop through all active pointers contained within this event.
                 * Data for each pointer is stored in a MotionEvent at an index
                 * (starting from 0 up to the number of active pointers). This
                 * loop goes through each of these active pointers, extracts its
                 * data (position and pressure) and updates its stored data. A
                 * pointer is identified by its pointer number which stays
                 * constant across touch events as long as it remains active.
                 * This identifier is used to keep track of a pointer across
                 * events.
                 */
                for (index = 0; index < event.getPointerCount(); index++) {
                    // Get pointer id for data stored at this index
                    id = event.getPointerId(index);
                    // Change the data
                    touchMove(id, event.getX(index), event.getY(index), event.getPressure(index));
                }
                return true;

            case MotionEvent.ACTION_CANCEL:
                /*
                 * Something unexpected happened.
                 */

                /*
                 * Remove all active touches.
                 */
                touchCancel();
                return true;

        }

        return false;
    }

}
