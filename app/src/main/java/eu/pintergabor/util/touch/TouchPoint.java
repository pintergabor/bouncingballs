package eu.pintergabor.util.touch;

/**
 * A point.
 */
@SuppressWarnings("unused")
public class TouchPoint {

    /**
     * X coordinate.
     */
    public float x;

    /**
     * Y coordinate.
     */
    public float y;

    /**
     * Pressure. (0.0 .. 1.0)
     */
    public float pressure;

    /**
     * ID. (Some number)
     */
    public int id;


    /**
     * Create an empty object.
     */
    public TouchPoint(){
        // Empty
    }

    /**
     * Create and initialize an object.
     *
     * @param x X coordinate.
     * @param y Y coordinate.
     * @param pressure Pressure.
     */
    @SuppressWarnings("unused")
    public TouchPoint(float x, float y, float pressure) {
        setTouch(x, y, pressure);
    }

    /**
     * Convenience method for setting coordinates and pressure at the same time.
     *
     * @param x X coordinate.
     * @param y Y coordinate.
     * @param pressure Pressure.
     */
    @SuppressWarnings("unused")
    public void setTouch(float x, float y, float pressure) {
        this.x = x;
        this.y = y;
        this.pressure = pressure;
    }

}
